<nav class="navbar navbar-expand-lg navbar-light bg-light p-1" style="border-bottom: 5px solid #1A90CD ">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarCollapse">
					<div class="container">
						<ul class="navbar-nav mr-auto align-items-center">
							<li class="nav-item">
								<a class="navbar-brand" href="../../index.html"><img class="rounded-circle shadow-sm" src="../../img/wixIMG/LogoAuvergne.png" width="50" alt="logo_auvergne"></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="../../index.html">ACCUEIL</a>
							</li>
							<li class="nav-item dropdown">
								<button class="btn btn-light nav-item nav-link dropdown-toggle" type="button" data-toggle="dropdown">MONTAGNE</button>
								<ul class="dropdown-menu">
									<li class="dropdown-item nav-item">
										<a class="nav-link" href="../../pages/montagne/menu.html">Menu</a>
									</li>
									<li class="dropdown-divider"></li>
									<li class="dropdown-item nav-item">
										<a class="nav-link" href="../../pages/montagne/skier.html">Skier toute l'année</a>
									</li>
									<li class=" dropdown-item nav-item">
										<a class="nav-link" href="../../pages/montagne/rando.html">Randonnées</a>
									</li>
									<li class=" dropdown-item nav-item">
										<a class="nav-link" href="../../pages/montagne/tyro.html">Tyroliennes géantes</a>
									</li>
								</ul>
							</li>
							<li class="nav-item dropdown">
								<button class="btn btn-light nav-item nav-link dropdown-toggle" type="button" data-toggle="dropdown">GASTRONOMIE</button>
								<ul class="dropdown-menu">
									<li class="dropdown-item nav-item">
										<a class="nav-link" href="../../pages/gastronomie/menu.html">Menu</a>
									</li>
									<li class="dropdown-divider"></li>
									<li class="dropdown-item nav-item">
										<a class="nav-link" href="../../pages/gastronomie/fromages.html">Fromages</a>
									</li>
									<li class=" dropdown-item nav-item">
										<a class="nav-link" href="../../pages/gastronomie/vins.html">Vins</a>
									</li>
								</ul>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="../../pages/sports/sports.html">SPORT</a>
							</li>
							<li class="nav-item dropdown">
								<button class="btn btn-light nav-item nav-link dropdown-toggle" type="button" data-toggle="dropdown">CULTURE</button>
								<ul class="dropdown-menu">
									<li class="dropdown-item nav-item">
										<a class="nav-link" href="../../pages/culture/menu.html">Menu</a>
									</li>
									<li class="dropdown-divider"></li>
									<li class="dropdown-item nav-item">
										<a class="nav-link" href="../../pages/culture/antiquite.html">Antiquité</a>
									</li>
									<li class=" dropdown-item nav-item">
										<a class="nav-link" href="../../pages/culture/romain.html">Chute de l'empire Romain</a>
									</li>
									<li class=" dropdown-item nav-item">
										<a class="nav-link" href="../../pages/culture/francs.html">L'auvergne sous les Francs</a>
									</li>
									<li class=" dropdown-item nav-item">
										<a class="nav-link" href="../../pages/culture/moyenage.html">Moyen Âge</a>
									</li>
								</ul>
							</li>
							<li class="nav-item dropdown">
								<button class="btn btn-light nav-item nav-link dropdown-toggle" type="button" data-toggle="dropdown">DECOUVERTE</button>
								<ul class="dropdown-menu">
									<li class="dropdown-item nav-item">
										<a class="nav-link" href="../../pages/decouverte/menu.html">Menu</a>
									</li>
									<li class="dropdown-divider"></li>
									<li class="dropdown-item nav-item">
										<a class="nav-link" href="../../pages/decouverte/annecy.html">Annecy</a>
									</li>
									<li class=" dropdown-item nav-item">
										<a class="nav-link" href="../../pages/decouverte/gorgesArdeche.html">Les gorges de l'Ardèche</a>
									</li>
									<li class=" dropdown-item nav-item">
										<a class="nav-link" href="../../pages/decouverte/puydome.html">Le Puy du Dôme</a>
									</li>
								</ul>
							</li>
							<li class="nav-item ">
								<a class="nav-link" href="../../pages/contact/contact.html">CONTACT</a>
							</li>
							<li class="ml-lg-0-auto">
								<a class="mx-2 " href="https://www.facebook.com/RegionAuvergneRhoneAlpes/"><img src="../../img/svg/facebook.svg" width="15" alt="logo_fb"></a>
								<a class="mx-2 " href="https://twitter.com/auvergnerhalpes"><img src="../../img/svg/twitter.svg" width="15" alt="logo_twitter"></a>
								<a class="mx-2 " href="https://www.instagram.com/auvergnerhonealpes.tourisme/"><img src="../../img/svg/instagram.svg" width="15" alt="logo_insta"></a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
